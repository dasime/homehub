# homehub

A node package for connecting to a BT Homehub. Tested and working with
BT Homehub 5A.

## Quick Start

Install it globally:

    npm install -g git+ssh://git@gitlab.com/dasime/homehub.git

Check your router's status with:

    homehub status -p $YOUR_ROUTER_ADMIN_PASSWORD

If it's working it'll return a table that might look something like:

    +--------------------+-----------------------------------+
    | connection start   | Sat Apr 21 2018 15:05:07 GMT+0100 |
    +--------------------+-----------------------------------+
    | connection uptime  | 1 day 8 hours 1 minute 41 seconds |
    +--------------------+-----------------------------------+
    | current up speed   | 19.999 Mbps                       |
    +--------------------+-----------------------------------+
    | current down speed | 73.85 Mbps                        |
    +--------------------+-----------------------------------+
    | max up speed       | 22.399 Mbps                       |
    +--------------------+-----------------------------------+
    | max down speed     | 76.223 Mbps                       |
    +--------------------+-----------------------------------+
    | data uploaded      | 437.0 MB                          |
    +--------------------+-----------------------------------+
    | data downloaded    | 16.3 GB                           |
    +--------------------+-----------------------------------+

## Disclaimer

Very little effort has been made to ensure this doesn't explode. I only have
one router to test this with so I can't guarantee this will work with your
Homehub 5A.
