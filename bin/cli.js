#!/usr/bin/env node
/* eslint-disable no-console */
const program = require('commander');
const Table = require('tty-table');

const package = require('../package.json');
const homehub = require('../lib/index.js');
const homehubHostnameDefault = 'http://192.168.1.254';

program
  .command('status')
  .description('return the current status of the router')
  .option('-h, --hostname [hostname]',
    'The domain of the router.',
    homehubHostnameDefault)
  .option('-p, --password <password>',
    'The password for the admin interface.')
  .action(options => {
    if (!options.password) {
      return console.error('Your router password is required to login. ' +
        'You can set this with the `-p` flag. See --help for more.');
    }
    homehub.getStatus(options.hostname, options.password)
      .then(status => {
        const table = new Table(
          [],
          [
            ['connection start', status.dslUptime.connectionStart],
            ['connection uptime', status.dslUptime.connectionDurationHuman],
            ['current up speed', status.currentDataRate.up],
            ['current down speed', status.currentDataRate.down],
            ['max up speed', status.maxDataRate.up],
            ['max down speed', status.maxDataRate.down],
            ['data uploaded', status.dataTransferred.sent],
            ['data downloaded', status.dataTransferred.recieved],
          ],
          {
            borderStyle: 2,
            align: 'left',
          }
        );
        console.log(table.render());
      })
      .catch(err => {
        console.error(`There was an error fetching the status: ${err}`);
      });
  });

program
  .usage('<command>')
  .version(package.version)
  .parse(process.argv);
