const api = require('./api');

/**
 * Gets the status.
 *
 * @param      {String}   homehubHostname  The homehub hostname
 * @param      {String}   homehubPassword  The homehub password
 * @return     {Promise}  An object containing the status.
 */
function getStatus (homehubHostname, homehubPassword) {
  const homehubUrl = `${homehubHostname.replace(/\/$/, '')}/index.cgi`;
  return new Promise ((resolve, reject) => {
    api.auth(homehubUrl, homehubPassword)
      .then(([jar, request_id]) => {
        api.status.getStatus(homehubUrl, jar, request_id)
          .then(status => {
            return resolve(status);
          })
          .catch(err => {
            return reject(err);
          });
      })
      .catch(err => {
        return reject(err);
      });
  });
}

module.exports = {
  getStatus,
};
