const cheerio = require('cheerio');
const crypto = require('crypto');
const request = require('request-promise-native').defaults({
  timeout: 10000,
});

/**
 * Authenticate to the HomeHub
 *
 * @param      {String}   homehubUrl       The homehub url
 * @param      {String}   homehubPassword  The homehub password
 * @return     {Promise}  [jarContainingCookies, request_id]
 */
function auth(homehubUrl, homehubPassword) {
  return new Promise((resolve, reject) => {
    const jar = request.jar();
    request({
      url: homehubUrl,
      method: 'GET',
      jar: jar,
      qs: {
        active_page: 9101,
        nav_clear: 1,
      },
    })
    .then(body => {
      const $ = cheerio.load(body);
      const authKey = $('[name="auth_key"]').val();
      const encryptedPassword = crypto.createHash('md5')
                                      .update(`${homehubPassword}${authKey}`)
                                      .digest('hex');

      const request_id = $('[name="request_id"]').val();
      request({
        url: homehubUrl,
        method: 'POST',
        jar: jar,
        form: {
          request_id: request_id,
          active_page: $('[name="active_page"]').val(),
          active_page_str: $('[name="active_page_str"]').val(),
          mimic_button_field: 'submit_button_login_submit: ..',
          button_value: '',
          post_token: $('[name="post_token"]').val(),
          // `name="password_${UUID}"`
          // Generated as part of the `#password` form field
          [$('#password').attr('name')]: '',
          md5_pass: encryptedPassword,
          auth_key: authKey,
        },
        simple: false, // We expect a 302, not a 200
      })
      .then(() => {
        // TODO: Does this 302 page actually change when there is an issue
        //       during the login? It should be possible to detect a failed
        //       login!
        return resolve([jar, request_id]);
      })
      .catch(err => {
        return reject('Error attempting login', err);
      });
    })
    .catch(err => {
      return reject('Error connecting to login page', err);
    });
  });
}

module.exports = auth;
