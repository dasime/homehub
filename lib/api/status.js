const cheerio = require('cheerio');
const moment = require('moment');
require('moment-precise-range-plugin');
const request = require('request-promise-native').defaults({
  timeout: 10000,
});

/**
 * Gets the status.
 *
 * @param      {String}   homehubUrl  The homehub url
 * @param      {Object}   jar         A request cookie jar containing cookies!
 * @param      {String}   request_id  The request identifier
 * @return     {Promise}  An object containing the status.
 */
function getStatus(homehubUrl, jar, request_id) {
  return new Promise((resolve, reject) => {
    request({
      url: homehubUrl,
      method: 'GET',
      jar: jar,
      qs: {
        active_page: 9143,
        request_id: request_id,
        nav_clear: 1,
      },
    })
    .then(body => {
      const $ = cheerio.load(body);

      //console.log('test', $('title').text())

      // "5. DSL uptime"
      // The time the router has been online is set in a variable
      // on the page called `wait` and updates the table with some lol code >_>.
      const connectionDurationSeconds = body.match(/wait\s?=\s?(\d+);/);

      if (!connectionDurationSeconds) {
        return reject('Unable to find connection uptime on status page.');
      }

      const now = moment();
      const connectionStart = moment(now).subtract(
        connectionDurationSeconds[1],
        'seconds'
      );

      // 6. Data rate
      const currentDataRate = retrieveSplitValue($, 'Data rate')
        .map(dataRate => formatMegaBits(dataRate));
      // 7. Maximum data rate
      const maxDataRate = retrieveSplitValue($, 'data rate')
        .map(dataRate => formatMegaBits(dataRate));
      // 11. Data sent/received:
      const dataTransferred = retrieveSplitValue($, 'sent/received');

      return resolve({
        dslUptime: {
          connectionDurationHuman: moment.preciseDiff(now, connectionStart),
          connectionStart: connectionStart,
        },
        currentDataRate: {
          up: currentDataRate[0],
          down: currentDataRate[1],
        },
        maxDataRate: {
          up: maxDataRate[0],
          down: maxDataRate[1],
        },
        dataTransferred: {
          sent: dataTransferred[0],
          recieved: dataTransferred[1],
        },
      });
    })
    .catch(err => {
      return reject(err);
    });
  });
}

/**
 * Retrieves a value from the status table and splits it on ` / `.
 *
 * @param      {Object}  $       The body of the page parsed by `cheerio`
 * @param      {String}  text    The text to find in the table
 * @return     {Array}   An array containing the split values from the cell.
 */
function retrieveSplitValue ($, text) {
  return $(`td:contains("${text}")`).last()
                                    .next('td')
                                    .text()
                                    .trim()
                                    .split(' / ');

}

/**
 * Format a data rate in Kbps as Mbps if appropriate and append a unit.
 *
 * @param      {Number}  dataRate  The data rate to format
 * @return     {String}  The formatted data rate with appended unit
 */
function formatMegaBits (dataRate) {
  return dataRate > 1000 ? `${dataRate / 1000} Mbps` : `${dataRate} Kbps`;
}

module.exports = {
  getStatus,
};
